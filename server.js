require('dotenv').config()

// console.log(__filename)
console.log(process.env.username1)
console.log(process.env.NODE_ENV)

//import the express lab that let me work with get,post,put,delet and more methods
const express = require('express')

//import the cors lab
const cors = require('cors')

//import the files lab
const fs = require('fs')

//a lab that bring random uniq id
const uniqid = require('uniqid')

//a var that hold all the express lab in him
const app = express()

//a lab that auto parse json from the body to string
app.use(express.json())

//make it's passible to run the program on web and not just in the postman
app.use(cors())

app.use(express.static('client'))

// let list = [
//   { id: "a1", text: "baba", done: false },
//     { id: "a2", text: "dishes", done: false },
//     { id: "a3", text: "runnig", done: false },
//     { id: "a4", text: "java", done: true },
//     { id: "a5", text: "css", done: false }
// ]


//creat new file
if (!fs.existsSync('list.json'))
  fs.writeFileSync('list.json', JSON.stringify([], null, 4))

//parse the object in list.json 
const json = require('./list')

//the server showing the tasks list
app.get('/list', function (req, res) {
  res.send(json)
})

//add new task
app.post('/list', function (req, res) {

  // let x = json.find(item => item.id == req.body.id)
  // if(!x){
  try {
    if (!req.body.text) { throw new Error('no text!') }
    json.push({ id: uniqid(), text: req.body.text, done: false })
    fs.writeFileSync('list.json', JSON.stringify(json, null, 4))
    res.send(json)

  }
  catch (err) {
    res.status('400').send(err.message)
  }
})

//change the done, from true to false and the oppsite
app.put('/list', function (req, res) {
  const index = json.findIndex(item => req.body.id == item.id)
  json[index].done = !json[index].done
  fs.writeFileSync('list.json', JSON.stringify(json, null, 4))
  res.send(json)
})

//delet task
app.delete('/list/:id', function (req, res) {
  // let del = req.params.id ;
  const inde = json.findIndex(item => item.id == req.params.id)
  json.splice(inde, 1)
  fs.writeFileSync('list.json', JSON.stringify(json, null, 4))
  res.send(json)
})


app.listen(3000)
console.log('server running!');