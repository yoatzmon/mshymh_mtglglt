function stringify(s) {
    localStorage.s = JSON.stringify(s)
}
function parse() {
    if (!localStorage.s)
        return []
    return JSON.parse(localStorage.s)
}
// stringify([
//     { id: "a1", text: "homework", done: false },
//     { id: "a2", text: "dishes", done: false },
//     { id: "a3", text: "runnig", done: false },
//     { id: "a4", text: "java", done: true },
//     { id: "a5", text: "css", done: false }
// ])

renderLists()
addTask()


function renderLists() {
    document.getElementById('before').innerHTML = ""
    document.getElementById('after').innerHTML = ""
    let list = parse()
    list.forEach(element => {
        if (!element.done) {
            document.getElementById('before').innerHTML +=
                `<li>${element.text}
            <input type="checkbox" onchange="updateTask('${element.id}')"/>
            <input type="button" value="X" onclick ="deletTask('${element.id}')">
            </li>`
            }
        else {
            document.getElementById('after').innerHTML +=
                `<li>${element.text}
                <input type="checkbox" checked onchange="updateTask('${element.id}')"/>
            </li>`
            
        }
        
    });
   
}
function addTask() {
    document.querySelector('form').onsubmit = event => {
        event.preventDefault()
        const values = Object.values(event.target)
            .reduce((acc, input) => !input.name ? acc : ({
                ...acc,
                [input.name]: input.type == 'checkbox' ? input.checked : input.value
            }), {}
            )
        let list = parse()
        list.push({ id: "a" + (Number(list.length) + 1), text: values.mition, done: false })
        stringify(list)
        renderLists()

    }
}
function updateTask(i) {
    let list = parse()
    list.forEach(item => {
        if (item.id == i) {
            if (item.done == false) {
                item.done = true
                stringify(list)
                renderLists()
                
            }
            else {
                item.done = false
                stringify(list)
                renderLists()
    
            }

        }
    })
}
function deletTask(n) {
    let list = parse()
    list.forEach((item, index) => {

        if (item.id == n) {
            list.splice(index, 1)
            stringify(list)
            renderLists()
        }
    });
}
