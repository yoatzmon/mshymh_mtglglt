renderLists()

addTask()

async function renderLists() {
    document.getElementById('before').innerHTML = ""
    document.getElementById('after').innerHTML = ""

    let res = await axios.get('http://localhost:3000/list')
        // .then(res => {
            const list = res.data;
            list.forEach(element => {
                // console.log(element);
                if (!element.done) {
                    document.getElementById('before').innerHTML +=
                        `<li>${element.text}
                            <input type="checkbox" onchange="updateTask('${element.id}')"/>
                            <input type="button" value="X" onclick ="deletTask('${element.id}')">
                        </li>`}
                else {
                    document.getElementById('after').innerHTML +=
                        `<li>${element.text}
                            <input type="checkbox" checked onchange="updateTask('${element.id}')"/>
                        </li>`
                }

            });
        // })
}
function addTask() {
    document.querySelector('form').onsubmit = event => {
        event.preventDefault()
        const values = Object.values(event.target)
            .reduce((acc, input) => !input.name ? acc : ({
                ...acc,
                [input.name]: input.type == 'checkbox' ? input.checked : input.value
            }), {}
            )
        axios.post('/list', { text: values.mition, done: false })
            .then(res => {
                const list = res.data;
                // list.push({ id: "a" + (Number(list.length) + 1), text: values.mition, done: false })
                renderLists()
            })
    }
}
async function updateTask(i) {
    await axios.put('/list',{id : i})
        // .then(res => {
            renderLists()
            // const list = res.data;
            // list.forEach(item => {
            //     if (item.id == i.id) {
            //         if (item.done == false) {
            //             item.done = true
            //             renderLists()
            //         }
            //         else {
            //             item.done = false
            //             renderLists()
            //         }
                    
            //     }
            // })
        // })
      
}
// function deletTask(n) {
//     axios.delete(`/list/${n}`)
//         .then(res => {
//             renderLists()
//     })
// }

async function deletTask(n) {
    await axios.delete(`/list/${n}`)
        // .then(res => {
            renderLists()
    // })
}

